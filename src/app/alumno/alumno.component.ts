import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-alumno',
  templateUrl: './alumno.component.html',
  styleUrls: ['./alumno.component.css']
})
export class AlumnoComponent{

  activo = true;
 // alumnos: string[] = ['Rogelio', 'Jorge', 'Ana'];
  @Input() alumnos: string[] = ['Arturo'];

  // tslint:disable-next-line:typedef
  onClickActivar(){
    this.activo = !this.activo;
  }

}

