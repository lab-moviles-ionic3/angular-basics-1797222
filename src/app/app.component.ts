import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AngularBasics1797222';

  alumnosDummy: string[] = ['barbara', 'armando'];

  // tslint:disable-next-line:typedef
  onAddAlumno(name: string){
    this.alumnosDummy.push(name);

  }
}
